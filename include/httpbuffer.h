#pragma once

#include "socketbuffer.h"

#include <list>

class httpbuffer : public socketbuffer
{
public:
	httpbuffer();
	~httpbuffer();

	bool parse_line();
	std::string pop();

	void append(const char *);
	void append(const char *, size_t);

private:

	size_t expected_length_;
	size_t observed_length_;
	size_t already_served_length_;
	bool content_length_detected_;
	bool start_observing_length_;
	std::list<std::string> parsed_chunks_;
};
