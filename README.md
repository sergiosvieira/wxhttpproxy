## wxHttpProxy

![wxhttpproxy screenshot](https://bitbucket.org/rayburgemeestre/wxhttpproxy/raw/master/docs/screenshot.png)

More features, editable cache, etc.:

![wxhttpproxy screenshot](https://bitbucket.org/rayburgemeestre/wxhttpproxy/raw/master/docs/screenshot2.png)

Console version:

    ksh$ wxhttpproxy --help
    Usage: wxhttpproxy [-h] [-f <str>] [-n <num>] [-p <num>] [-q]
      -h, --help            displays help on the command line parameters.
      -f, --file=<str>      output everything to <file>.
      -n, --number=<num>    truncate file each <num> lines (default 500000).
      -p, --port=<num>      bind to port <port> (default 8888).
      -q, --quiet           silent output (nothing to stdout).

Example usage (`http_proxy=http://localhost:8888 curl http://cppse.nl/test.txt`)

    ksh$ wxhttpproxy
    Info - Server listening on port 8888.
    Thread #1 - New client connection accepted
    Thread #1 - Connecting to host cppse.nl:80 for client.
    Thread #1 - Request from client: GET /test.txt HTTP/1.1
    Thread #1 - Host socket connected.
    Thread #1 - >>>: GET /test.txt HTTP/1.1
    Thread #1 - >>>: User-Agent: curl/7.30.0
    Thread #1 - >>>: Host: cppse.nl
    Thread #1 - >>>: Accept: */*
    Thread #1 - >>>: Proxy-Connection: Keep-Alive
    Thread #1 - >>>:
    Thread #1 - Response from host.
    Thread #1 - <<<: HTTP/1.1 200 OK
    Thread #1 - <<<: Date: Sun, 28 Dec 2014 21:05:09 GMT
    Thread #1 - <<<: Server: Apache/2.4.6 (Ubuntu)
    Thread #1 - <<<: Last-Modified: Sun, 09 Feb 2014 00:48:33 GMT
    Thread #1 - <<<: ETag: "c-4f1ee951f920c"
    Thread #1 - <<<: Accept-Ranges: bytes
    Thread #1 - <<<: Content-Length: 12
    Thread #1 - <<<: Connection: close
    Thread #1 - <<<: Content-Type: text/plain
    Thread #1 - <<<:
    Thread #1 - <<<: Hello world
    Thread #1 - Host socket disconnected.

## Description

As a free alternative for Charles proxy and/or Fiddler I developed wxHttpProxy
in one (long) night!
Unfortunately I had to spend the next day making it stable, and support https
traffic as well ;)

It works great as a tool by itself. Although the output could be improved off
course.  The idea is to integrate the http proxy into metalogmon, so that you
would be able to process your output through javascript.

The reason that request/response are both first class citizens is that where I
currently develop software I plan to use this proxy between all subsystems
communication, and here multiple services communicate with eachother using REST
calls.  This way I can later (in metalogmon) visualize the call hierarchy (A
requests B, which requests C, D, gets the responses, and then responds to A).

Before deciding to make one myself I attempted to modify tinyproxy, but it
would become to hacky. And as it's not very complex decided to implement one
from scratch.

