#include "consoleoutputhandler.h"
#include <iostream>
#include <wx/string.h>

bool ConsoleOutputHandler::UseCache()
{
    return false;
}


void ConsoleOutputHandler::CacheUri(wxString &uri)
{
    std::cout << "INFO - Caching request \"" << uri.c_str() << "\"" << std::endl;
}

std::string ConsoleOutputHandler::FindUri(long item)
{
    return "";
}

void ConsoleOutputHandler::ClearUris()
{

}
