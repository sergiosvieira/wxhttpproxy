#include "cache.h"


#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#include "cache.h"

#endif

#include "outputhandler.h"

std::string normalize_request_uri(const std::string &in)
{
	wxString temp(in.c_str(), wxConvUTF8);
	temp.Replace("\r", "", true);
	temp.Replace("\n", "", true);
	std::string key = (const char *)temp.c_str();
	return std::move(key);
}


Cache::Cache(OutputHandler &outputHandler)
: outputHandler_(outputHandler)
{
}

std::shared_ptr<CachedRequest> Cache::GetCachedRequestByUri(const std::string &uri)
{
#if wxUSE_GUI == 0
	return std::shared_ptr<CachedRequest>(new CachedRequest(""));
#endif

	if (!outputHandler_.UseCache())
		return std::shared_ptr<CachedRequest>(new CachedRequest(""));

	std::string key = normalize_request_uri(uri);

	std::cout << "Finding cache [" << key << "]" << std::endl;
	if (cachedRequestsMap_.find(key) != cachedRequestsMap_.end()) {
		return cachedRequestsMap_[key];
	}

	return std::shared_ptr<CachedRequest>(new CachedRequest(""));
}

std::shared_ptr<CachedRequest> Cache::CreateNewRequestForUri(const std::string &uri)
{
#if wxUSE_GUI == 0
	return std::shared_ptr<CachedRequest>(new CachedRequest(""));
#endif

	if (!outputHandler_.UseCache())
		return std::shared_ptr<CachedRequest>(new CachedRequest(""));

	std::string key = normalize_request_uri(uri);

	std::cout << "Creating cache [" << key << "]" << std::endl;

	cachedRequestsMap_[key] = std::shared_ptr<CachedRequest>(new CachedRequest(key));
	return cachedRequestsMap_[key];
}

void Cache::RegisterCachedRequestInGuiListCtrl(std::shared_ptr<CachedRequest> cache)
{
#if wxUSE_GUI == 0
	return;
#endif
	if (cache.get()->uri() == "" || !outputHandler_.UseCache())
		return;

	outputHandler_.CacheUri(wxString(cache->uri().c_str(), wxConvUTF8).Trim(true));
}

std::shared_ptr<CachedRequest> Cache::GetCachedRequestByListCtrlIndex(long item)
{
#if wxUSE_GUI == 0
	return std::shared_ptr<CachedRequest>(new CachedRequest(""));
#endif

	if (!outputHandler_.UseCache())
		return std::shared_ptr<CachedRequest>(new CachedRequest(""));

	std::string uri = outputHandler_.FindUri(item);

	uri = normalize_request_uri(uri);

	if (!uri.empty()) {
		return cachedRequestsMap_[uri];
	}

    return std::shared_ptr<CachedRequest>(new CachedRequest(""));
}


void Cache::ClearAll()
{
	cachedRequestsMap_.clear();
	outputHandler_.ClearUris();
}
