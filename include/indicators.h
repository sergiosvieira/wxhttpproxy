#pragma once

#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include <map>

class Indicators
{
public:
	
	virtual size_t AddIndicator() = 0;
	
	virtual void RemoveIndicator(size_t indicator) = 0;
	
	virtual void SetIndicatorLabel(size_t indicator, wxString label) = 0;
	
	virtual void SetSecure(size_t indicator) = 0;

	#if wxUSE_GUI == 1
	size_t indicatorPanelIndex_;
	std::map<size_t, wxPanel *> indicatorPanels_;
	std::map<size_t, wxStaticText *> indicatorLabels_;

protected:

	Indicators() : indicatorPanelIndex_(0) {}
	#endif

};
