#pragma once

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "wx/socket.h"

#include <map>
#include "logger.h"
#include "cache.h"

class wxSocketServer;
class ClientData;
class Indicators;
class OutputHandler;

/**
 * wxFrame is necessary for the Event Table..
 */
class SocketServer : public wxEvtHandler, public Cache
{
public:

	enum IDS {
		SERVER_ID = 1000,
		SOCKET_ID,
		HOST_ID
	};

	SocketServer(
		int port,
		OutputHandler &outputHandler,
		Logger &logger
	);
	
	virtual ~SocketServer();

	Logger &GetLogger() 
	{
		return logger_;
	}

private:

	void OnServerEvent(wxSocketEvent& event);
	void OnSocketEvent(wxSocketEvent& event);
	void OnHostClientEvent(wxSocketEvent& event);


	OutputHandler &outputHandler_;

	Logger &logger_;
	wxSocketServer socketServer_;
	Indicators &indicators_;

	// Somehow I had problems with the SetClientData(), 
	//  so I'll keep track of the objects myself..
	std::map<int, ClientData *> clientData_;

	std::map<int, size_t> mapClientsToIndicators_;

	DECLARE_EVENT_TABLE()
};
