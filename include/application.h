/////////////////////////////////////////////////////////////////////////////
// Name:        application.h
// Purpose:     
// Author:      Ray Burgemeestre
// Modified by: 
// Created:     Wed 06 Nov 2013 21:57:53 CET
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#pragma once

/*!
 * Includes
 */

#include <memory>

#if wxUSE_GUI == 1
////@begin includes
#include "wx/image.h"
#include "httpproxywindow.h"
////@end includes
#define wxAppConsole wxApp
#endif

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
////@end control identifiers

/*!
 * Application class declaration
 */

class SocketServer;

#include "outputhandler.h"
#include "logger.h"

class Application : public wxAppConsole {
	DECLARE_CLASS(Application)
	DECLARE_EVENT_TABLE()

public:
	/// Constructor
	Application();

	void Init();

	/// Initialises the application
	virtual bool OnInit();

	/// Called on exit
	virtual int OnExit();

    bool UsesEventLoop();

	bool ProcessCommandline();

	////@begin Application event handler declarations

	////@end Application event handler declarations

	////@begin Application member function declarations

	////@end Application member function declarations

	////@begin Application member variables
	////@end Application member variables
#if wxUSE_GUI == 0
	virtual int OnRun();
#endif

	std::unique_ptr<Logger> logger_;

	std::unique_ptr<OutputHandler> outputHandler_;

	std::unique_ptr<SocketServer> socketServer_;

	std::string outputFile_;
	long outputFileTruncateLines_;
	unsigned short bindPort_;
	bool quiet_;

};

/*!
 * Application instance declaration 
 */

////@begin declare app
DECLARE_APP(Application)
////@end declare app

#undef wxAppConsole
#define wxAppConsole wxAppConsole

// _APPLICATION_H_
