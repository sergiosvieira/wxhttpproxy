#pragma once

#include <string>

class socketbuffer
{
public:
	socketbuffer();
	~socketbuffer();

	virtual void append(const char *);
	virtual void append(const char *, size_t);

	const std::string &get();
	size_t length();

	bool has_line();
	std::string get_line();
	std::string get_raw();


protected:

	std::string buffer_;
};
